class FilterModule(object):

    def filters(self):
        return {
            'inventories_filter': self.inventories_filter,
            'host_filter': self.host_filter,
        }

    def host_filter(self, input):
      print(input)
      return 'hoi'

    def inventories_filter(self, input):
      print(input)
      groups = []

      import re

      for x in input:

        splitted = re.split("_", x)
        print(splitted)
        if len(splitted) == 1:
          return [splitted]

        customer = ''

        env = splitted[0]
        app = splitted[1]
        comp = splitted[2] if len(splitted) > 2 else ''
        custnum = splitted[3] if len(splitted) > 3 else ''
        instnum = splitted[4] if len(splitted) > 4 else ''

        # customer
        if custnum == '100':
          customer = 'example'
        if custnum == '200':
          customer = 'swica'
        if custnum == '300':
          customer = 'helsana'

      if env and custnum and instnum:
        groups.append(env + '_' + custnum + '_' + instnum)

      if env:
        groups.append('a1_env_' + env)

      if app:
        groups.append('a2_app_' + app)

      if customer:
        groups.append('a3_cust_' + customer)

      if comp and not bool(re.search(r'\d', comp)):
        groups.append('a4_comp_' + comp)

      if app and customer:
        groups.append('a5_' + customer + '_' + app)

      if env and customer:
        groups.append('a6_' + customer + '_' + env)

#      x = { 'pascal':  { 'children': [ 'hallo', 'hoi' ], } }
#      print(x)
#      return(x)

#      print(groups)
      return groups
